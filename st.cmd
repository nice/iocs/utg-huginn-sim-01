#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require(huginn)
 
# -----------------------------------------------------------------------------
# Utgard-Lab
# -----------------------------------------------------------------------------
epicsEnvSet("LOCATION",             "Utgard")
epicsEnvSet("SYS",                  "SIM:")
epicsEnvSet("DEV",                  "HUGINN-01")
epicsEnvSet("PREFIX",               "$(SYS)$(DEV)")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(huginn_DIR)db/")
# -----------------------------------------------------------------------------
# cryostat sub (sample)
epicsEnvSet("CRYO_SUB",  "SIM:LS336-01")
epicsEnvSet("T_BASE",               "KRDG0")
epicsEnvSet("T_PELTIER_TOP",        "KRDG2")
epicsEnvSet("T_PELTIER_BOTTOM",     "KRDG3")
epicsEnvSet("T_SAMPLE",             "KRDG1")
# -----------------------------------------------------------------------------
# cryostat main
epicsEnvSet("CRYO_MAIN",  "SIM:LS336-02")
epicsEnvSet("T_MAIN_BASE",          "KRDG1")
epicsEnvSet("T_MAIN_HEATER",        "KRDG0")
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Loading databases
# -----------------------------------------------------------------------------
# Huginn
iocshLoad("$(huginn_DIR)huginn.iocsh")

# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# -----------------------------------------------------------------------------
# Huginn and LakeShores have some default parameters loaded when the IOC starts
# -----------------------------------------------------------------------------
# Calling a script which copies default settings .sav to specific AS_TOP
# input params:
#     $1 AUTOSAVE_DIR
#     $2 AUTOSAVE_FILE
# -----------------------------------------------------------------------------
epicsEnvSet("AUTOSAVE_DIR",   "$(AS_TOP)/$(IOCNAME)/save")
epicsEnvSet("AUTOSAVE_FILE",  "$(huginn_DIR)defaultHuginnSettings.sav")
system "$(huginn_DIR)huginnCopyAutosaveSettings.sh $(AUTOSAVE_DIR) $(AUTOSAVE_FILE)"


# -----------------------------------------------------------------------------
# Huginn settings being restored
# -----------------------------------------------------------------------------
# this line will restore data after initialization of array, during pass0 memory for the array is not yet allocated
set_pass1_restoreFile("$(AUTOSAVE_FILE)", "P=$(PREFIX), CRYO_SUB=$(CRYO_SUB), CRYO_MAIN=$(CRYO_MAIN)")
# Do not create datedBeckupfiles every time when ioc stops. It will create only a backaup file of myArrays.sav named .sav.bu
save_restoreSet_DatedBackupFiles(0)

# -----------------------------------------------------------------------------
# Starting sequencer program
# -----------------------------------------------------------------------------
seq snl_huginn("P=$(PREFIX), CRYO_SUB=$(CRYO_SUB), T_BASE=$(T_BASE)")

# -----------------------------------------------------------------------------
# Starting the IOC
# -----------------------------------------------------------------------------
iocInit()

# Necessary in simulations
dbpf $(PREFIX):Limits.INPD "$(CRYO_MAIN):KRDG0 CPP"

